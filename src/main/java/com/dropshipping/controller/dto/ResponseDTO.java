package com.dropshipping.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ResponseDTO implements Serializable {

    private static final long serialVersionUID = 537520105234794609L;

    private String id;
    private String username;
    private String password;
    private String message;
}
