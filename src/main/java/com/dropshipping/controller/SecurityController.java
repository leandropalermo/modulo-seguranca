package com.dropshipping.controller;

import com.dropshipping.controller.dto.ResponseDTO;
import com.dropshipping.controller.exception.InvalidTokenException;
import com.dropshipping.service.GeneratePasswordService;
import com.dropshipping.service.TokenAuthenticationService;
import com.dropshipping.service.UserAuthenticationService;
import com.dropshipping.service.userdetail.User;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/users")
public class SecurityController {

    @Autowired
    private UserAuthenticationService authentication;

    @Autowired
    private GeneratePasswordService generatePasswordService;

    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    @PostMapping("/token/{username}")
    public String findTokenByUser(@PathVariable("username") String username) {
        Optional<String> token = authentication.token(username);
        return token.orElseThrow(() -> new RuntimeException("Error in creating a new token."));
    }

    @PostMapping("/password")
    public Optional<String> getPassword(@RequestBody ResponseDTO responseDTO) {
        return generatePasswordService.generatePassword(responseDTO);
    }

    @GetMapping("/verify/token/{token}")
    User verifyToken(@PathVariable("token") String token) throws InvalidTokenException{
        boolean isTokenExpired = tokenAuthenticationService.verifyIsTokenExpired(token);
        if (isTokenExpired) {
            throw new InvalidTokenException("Token[" + token + "] is not valid.");
        }
        Optional<User> byUsername = tokenAuthenticationService.findUsernameByToken(token);
        if(!byUsername.isPresent()) {
            throw new InvalidTokenException("Cannot find user with authentication token=" + token);
        }
        return byUsername.get();
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler({ExpiredJwtException.class})
    private ResponseDTO tokenExpired(ExpiredJwtException e) {
        return ResponseDTO.builder().message("Token expired").build();
    }

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler({InvalidTokenException.class})
    private ResponseDTO tokenExpired(InvalidTokenException e) {
        return ResponseDTO.builder().message(e.getMessage()).build();
    }
}
