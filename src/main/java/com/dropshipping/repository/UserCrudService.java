package com.dropshipping.repository;

import com.dropshipping.service.userdetail.User;

import java.util.Optional;

public interface UserCrudService {
    User save(User user);

    Optional<User> find(String id);

    Optional<User> findByUsername(String username);
}
