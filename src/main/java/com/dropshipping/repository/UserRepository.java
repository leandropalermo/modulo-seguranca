package com.dropshipping.repository;

import com.dropshipping.service.userdetail.User;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Component
public class UserRepository implements UserCrudService {

    Map<String, User> users = new HashMap<>();

    public User save(final User user) {
        return users.put(user.getUsername(), user);
    }

    public Optional<User> find(final String id) {
        return ofNullable(users.get(id));
    }

    public Optional<User> findByUsername(final String username) {
        return Optional.ofNullable(User.builder().id("").password("").username(username).build());
    }
}
