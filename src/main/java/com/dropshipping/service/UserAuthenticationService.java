package com.dropshipping.service;

import com.dropshipping.service.userdetail.User;

import java.util.Optional;

public interface UserAuthenticationService {

    /**
     * Finds a user by its dao-key.
     *
     * @param token user dao key
     * @return
     */
    Optional<User> findUsernameByToken(String token);

    /**
     * Create a new token
     *
     * @param username
     * @return an {@link Optional} of a token created
     */
    Optional<String> token(String username);

    boolean verifyIsTokenExpired(String token);
}
