package com.dropshipping.service;

import com.dropshipping.repository.UserCrudService;
import com.dropshipping.service.userdetail.User;
import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@Service
@AllArgsConstructor(access = PACKAGE)
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class TokenAuthenticationService implements UserAuthenticationService {

    @NonNull
    TokenService tokens;

    @NonNull
    UserCrudService users;

    @Override
    public Optional<User> findUsernameByToken(String token) {

        return Optional
                .of(tokens.findUsernameByToken(token))
                .map(map -> map.get("username"))
                .flatMap(users::findByUsername);
    }

    @Override
    public Optional<String> token(String username) {
        return Optional.ofNullable(tokens.expiring(ImmutableMap.of("username", username)));
    }

    @Override
    public boolean verifyIsTokenExpired(String token) {
        return tokens.verifyIsTokenExpired(token);
    }
}
