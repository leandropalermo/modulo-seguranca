package com.dropshipping.service;

import java.util.Map;

public interface TokenService {

    String expiring(Map<String, String> attributes);

    boolean verifyIsTokenExpired(String token);

    Map<String, String> findUsernameByToken(final String token);
}
