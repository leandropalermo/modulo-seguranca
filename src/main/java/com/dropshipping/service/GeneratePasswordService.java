package com.dropshipping.service;

import com.dropshipping.controller.dto.ResponseDTO;

import java.util.Optional;

public interface GeneratePasswordService {

    Optional<String> generatePassword(ResponseDTO responseDTO);
}
