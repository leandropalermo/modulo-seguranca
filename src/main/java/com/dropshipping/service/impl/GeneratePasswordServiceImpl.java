package com.dropshipping.service.impl;

import com.dropshipping.controller.dto.ResponseDTO;
import com.dropshipping.service.GeneratePasswordService;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Service
public class GeneratePasswordServiceImpl implements GeneratePasswordService {

    @Value("${jwt.secret:secret}")
    private String secret;

    @Override
    public Optional<String> generatePassword(ResponseDTO responseDTO) {
        HashCode hashCode = Hashing.sha256()
                .hashString(responseDTO.getUsername() + responseDTO.getPassword() + secret, StandardCharsets.UTF_8);
        Optional<String> password = Optional.ofNullable(hashCode.toString());
        return password;
    }
}
