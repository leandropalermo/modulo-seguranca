package com.dropshipping.service;

import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableMap;
import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.compression.GzipCompressionCodec;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;
import static io.jsonwebtoken.impl.TextCodec.BASE64;
import static java.util.Objects.requireNonNull;

@Service
public class JWTTokenService implements Clock, TokenService {
    private static final GzipCompressionCodec COMPRESSION_CODEC = new GzipCompressionCodec();

    String issuer;
    int expirationSec;
    int clockSkewSec;
    String secretKey;

    JWTTokenService(
            @Value("${jwt.issuer:tcc}") final String issuer,
            @Value("${security.jwt.expiration}") final int expirationSec,
            @Value("${jwt.clock-skew-sec:300}") final int clockSkewSec,
            @Value("${security.jwt.secret}") final String secret) {
        super();
        this.issuer = requireNonNull(issuer);
        this.expirationSec = requireNonNull(expirationSec);
        this.clockSkewSec = requireNonNull(clockSkewSec);
        this.secretKey = BASE64.encode(requireNonNull(secret));
    }

    @Override
    public String expiring(final Map<String, String> attributes) {
        return newToken(attributes, expirationSec);
    }

    private String newToken(final Map<String, String> attributes, final int expiresInSec) {
        final Claims claims = Jwts
                .claims()
                .setIssuer(issuer)
                .setIssuedAt(now(LocalDateTime.now()))
                .setExpiration(now(LocalDateTime.now().plusSeconds(expiresInSec)));

        if (expiresInSec > 0) {
            claims.setExpiration(now(LocalDateTime.now().plusSeconds(expiresInSec)));
        }
        claims.putAll(attributes);

        return Jwts
                .builder()
                .setClaims(claims)
                .signWith(HS256, secretKey)
                .compressWith(COMPRESSION_CODEC)
                .compact();
    }

    @Override
    public boolean verifyIsTokenExpired(final String token) throws ExpiredJwtException, UsernameNotFoundException {
        Optional<Object> isExpired = Optional.ofNullable(Jwts.parser()
                .setSigningKey(secretKey)
                .requireIssuer(issuer)
                .parseClaimsJws(token));

        return !isExpired.isPresent();
    }

    @Override
    public Map<String, String> findUsernameByToken(final String token) {
        final JwtParser parser = Jwts
                .parser()
                .requireIssuer(issuer)
                .setClock(this)
                .setAllowedClockSkewSeconds(clockSkewSec)
                .setSigningKey(secretKey);
        return parseClaims(() -> parser.parseClaimsJws(token).getBody());
    }

    private static Map<String, String> parseClaims(final Supplier<Claims> toClaims) {
        try {
            final Claims claims = toClaims.get();
            final ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
            for (final Map.Entry<String, Object> e : claims.entrySet()) {
                builder.put(e.getKey(), String.valueOf(e.getValue()));
            }
            return builder.build();
        } catch (final IllegalArgumentException | JwtException e) {
            return ImmutableMap.of();
        }
    }

    @Override
    public Date now() {
        return now(LocalDateTime.now());
    }

    public Date now(LocalDateTime now) {
        Date dt = java.util.Date.from(now.toLocalDate().atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
        return dt;
    }
}